## Updated: 01/09/2024 16:01:28 PM

## Info

- Last tag: 3.16.2-1.6.15-1
- Released: 11

## Versions

- Version: 3.16.2-1.6.15-1 (01/09/2024)
- Version: 3.16.2-1.6.14-1 (30/07/2024)
- Version: 3.16.2-1.6.13-1 (15/07/2024)
- Version: 3.16.2-1.6.12-1 (04/07/2024)
- Version: 3.16.2-1.6.11-2 (28/06/2024)
- Version: 3.16.2-1.6.11-1 (27/06/2024)
- Version: 3.16.2-1.6.5-1 (22/04/2024)
- Version: 3.16.2-1.6.2-1 (26/12/2023)
- Version: 3.16.2-1.6.0-1 (26/12/2023)
- Version: 3.13.5-1.4.0-1 (26/12/2023)
- Version: 3.3.10-1.3.4-1.11.2 (26/12/2023)

### Version: 3.16.2-1.6.15-1 (01/09/2024)

#### Feature

- Added FlutterAuroraGetDevicePixelRatio() function to C API.
- Added basic plugin implementation for ClientWrapper.

### Version: 3.16.2-1.6.14-1 (30/07/2024)

#### Feature

- Added C API for OpenGL texture.
- Added C API for getting EGL context.

#### Bug
- Enabled RTTI for ClientWrapper library. Pigeon (https://pub.dev/packages/pigeon) is work for Aurora plugin now.
- Fixed a linking error for GetStatusbarVisibilityMode() and SetStatusbarVisibilityMode() functions in Embedder C++ API.

### Version: 3.16.2-1.6.13-1 (15/07/2024)

#### Feature

- Add support SafeArea widget.
- System statusbar visibility.

### Version: 3.16.2-1.6.12-1 (04/07/2024)

#### Feature

- Add support application lifecycle events.

#### Bug

- Fixed bug with compilation on x86_64

### Version: 3.16.2-1.6.11-2 (28/06/2024)

#### Change

- Release build.

### Version: 3.16.2-1.6.11-1 (27/06/2024)

#### Bug

- Bug fixes.

#### Change

- General project refactoring.

### Version: 3.16.2-1.6.5-1 (22/04/2024)

#### Feature

- Add client wrapper support.
- Add changeln config and templates.

### Version: 3.16.2-1.6.2-1 (26/12/2023)

#### Bug

- Fix uninitialized texture registrar.
- Interpret an empty byte array as standard null type.

### Version: 3.16.2-1.6.0-1 (26/12/2023)

#### Bug

- Fix typos.

#### Change

- Clean up extra spaces.

#### Feature

- Add support flutter texture.
- Add support for application orientation locking.

### Version: 3.13.5-1.4.0-1 (26/12/2023)

#### Bug

- Fix pixel ratio reading.

#### Change

- Remove qsource from QStandardPaths::writableLocation.

#### Feature

- Add public methods to minimize and maximize the application window.

### Version: 3.3.10-1.3.4-1.11.2 (26/12/2023)

#### Change

- First release of Flutter SDK 3.3.10
