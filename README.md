# Flutter Embedder

![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F53351457%2Frepository%2Ftags)

[Flutter Embedder](https://docs.flutter.dev/embedded) — это новая разрабатываемая среда выполнения для приложений Flutter на ОС Аврора.

Этот репозиторий содержит **сборки** Flutter Embedder для [Аврора Platform SDK](https://developer.auroraos.ru/doc/software_development/psdk) необходимых для сборки и работы приложений Flutter на ОС Аврора.
В CHANGELOG.md вы найдете упорядоченный список версий проекта с датами их выхода, а также перечень всех изменений для каждой из версий.
